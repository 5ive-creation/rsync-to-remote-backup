#!/usr/bin/env python
from setuptools import setup

setup(
    name="rsync_to_remote_backup",
    version="1.5.4",
    author="kmogged",
    description="Scheduled configuration based rsync copies",
    packages=["rsync_to_remote_backup"],
    entry_points={
        "console_scripts": [
            "rsync-to-remote-backup=rsync_to_remote_backup.rsync_to_remote_backup:main"
        ]
    },
)
