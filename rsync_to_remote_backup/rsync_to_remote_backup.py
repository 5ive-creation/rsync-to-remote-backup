#!/usr/bin/python3 -u
import os
import argparse
from subprocess import Popen, run, CalledProcessError, TimeoutExpired
import configparser
from datetime import datetime, timedelta
from threading import Thread
import threading
from time import sleep
import queue


class Application:
    def read_config(self, path):
        config = configparser.ConfigParser()
        path = os.path.normpath(os.path.expanduser(path))
        if os.path.exists(path):
            config.read(path)
        else:
            print("ERROR: configuration file not found:", path)
            exit(1)
        return config

    def get_test_file_path(self, path):
        return os.path.join(
            path,
            ".rsync_to_remote_backup_directory",
        )

    def directory_is_approved(self, path, error_queue):
        # TODO: workaround for when the source is remote
        if "@" in path and ":" in path:
            return True

        test_file = self.get_test_file_path(path)
        if not os.path.exists(test_file):
            error_queue.put(
                "ERROR: test file missing. perform command : touch {} : to approve this directory for sync operation".format(
                    test_file
                )
            )
            return False
        return True

    def process_section(self, section, args, error_queue):
        stop_time = args.stop_time
        if not args.no_stop_time and not stop_time:
            hour = int(section.get("stop_time", "06:00").split(":")[0])
            minute = int(section.get("stop_time", "06:00").split(":")[1])
            stop_time = datetime.now().replace(hour=hour, minute=minute)
        if not args.no_stop_time and stop_time <= datetime.now():
            print("WARNING: stop time is not in the future", flush=True)
            return

        base_command = [
            "rsync",
            "-ar",
            "--partial",
            "--partial-dir=.rsync-partial",
            "--verbose",
            "-H",
        ]

        if not args.no_stop_time:
            stop_time_string = stop_time.strftime("%Y-%m-%dT%H:%M")
            print("INFO: will end copying at " + stop_time_string, flush=True)
            base_command.append("--stop-at={}".format(stop_time_string))

        if not section.getboolean("delete_before") and section.getboolean("delete"):
            base_command.append("--delete-after")

        if section.getboolean("delete_before"):
            base_command.append("--delete-before")

        if section.getboolean("compress", True):
            base_command.append("-z")

        if args.progress:
            base_command.append("--progress")
            base_command.remove("--verbose")

        if section.get("pre_command", False):
            process = Popen(section.get("pre_command").split(" "))
            process.communicate()

        for item in section:
            if item.startswith("source"):
                if not self.directory_is_approved(
                    section.get(item, "/invalid_rsync_to_remote_backup_directory"),
                    error_queue,
                ):
                    continue

                command = base_command.copy()
                command.append(section.get(item))
                destination = item.replace("source", "destination")
                if destination not in section:
                    error_queue.put(
                        "ERROR: misconfigured section {section:}. missing entry <Destination: {destination}>".format(
                            section=section, destination=destination
                        )
                    )
                    continue
                command.append(section.get(destination))

                if not args.no_stop_time and stop_time <= (
                    datetime.now() - timedelta(seconds=args.drift)
                ):
                    print("WARNING: exceeded stop time", flush=True)
                    if section.get("post_command", False):
                        process = Popen(section.get("post_command").split(" "))
                        process.communicate()
                    return
                if not args.no_stop_time:
                    timeout_seconds = (stop_time - datetime.now()).total_seconds()
                print(
                    "INFO: {source:} -> {destination:}".format(
                        source=section.get(item), destination=section.get(destination)
                    ),
                    flush=True,
                )
                if args.verbose:
                    print("INFO: {}".format(" ".join(command)))
                try:
                    if args.no_stop_time:
                        run(command, check=True, capture_output=True)
                        continue
                    run(
                        command,
                        timeout=timeout_seconds,
                        check=True,
                        capture_output=True,
                    )
                except CalledProcessError as e:
                    if e.returncode == 30:
                        print(
                            "WARNING: {section} stopped at requested limit for command {command}".format(
                                section=section, command=" ".join(e.cmd)
                            ),
                            flush=True,
                        )
                        if section.get("post_command", False):
                            process = Popen(section.get("post_command").split(" "))
                            process.communicate()
                        return
                    if e.returncode == 23:
                        error_output = e.stderr.decode() if e.stderr else ""
                        if (
                            "Time value of" in error_output
                            and "truncated on receiver" in error_output
                        ):
                            print(
                                "WARNING: {section} time value truncated on receiver for command {command}".format(
                                    section=section, command=" ".join(e.cmd)
                                ),
                                flush=True,
                            )
                            continue
                    error_queue.put(
                        "ERROR: {section} {error_code} {command}".format(
                            section=section,
                            error_code=e.returncode,
                            command=" ".join(e.cmd),
                        )
                    )
                    if section.getboolean("retry"):
                        if section.get("post_command", False):
                            process = Popen(section.get("post_command").split(" "))
                            process.communicate()
                        return self.process_section(section, args, error_queue)
                except TimeoutExpired as e:
                    print(
                        "WARNING: {section} stopped at requested limit for command {command}".format(
                            section=section,
                            command=" ".join(e.cmd),
                        ),
                        flush=True,
                    )
        if section.get("post_command", False):
            process = Popen(section.get("post_command").split(" "))
            process.communicate()
        return

    def wait_for_single_thread(self, args):
        sleep_increment = 5
        while threading.active_count() > 1:
            if args.verbose:
                print(
                    "INFO: waiting for active background threads({}) to complete".format(
                        threading.active_count() - 1
                    )
                )
            sleep(sleep_increment)

    def process_config(self, config, args):
        error_queue = queue.Queue()
        for job in config.sections():
            if args.section and job not in args.section:
                continue
            if config[job].getboolean("wait_for_single_thread", False):
                self.wait_for_single_thread(args)
            if config[job].getboolean("background", False):
                print("{}(background)".format(config[job]), flush=True)
                Thread(
                    target=self.process_section, args=(config[job], args, error_queue)
                ).start()
                continue
            print("{}".format(config[job]), flush=True)
            self.process_section(config[job], args, error_queue)

        self.wait_for_single_thread(args)

        if not error_queue.empty():
            while not error_queue.empty():
                print(error_queue.get(), flush=True)
            exit(1)

    def __init__(self):
        parser = argparse.ArgumentParser(
            description="rsync a list of tasks from a configuration",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "-c",
            "--config-file",
            help="configuration file",
            default=os.path.join(
                os.path.expanduser("~"), ".config", "rsync-to-remote-backup.conf"
            ),
        )
        parser.add_argument(
            "-s",
            "--stop-time",
            help="set the max time to run all configurations",
        )
        parser.add_argument(
            "--no-stop-time",
            help="run configurations without stopping before completion",
            action="store_true",
        )
        parser.add_argument(
            "-v",
            "--verbose",
            help="more verbose output",
            action="store_true",
        )
        parser.add_argument(
            "--progress",
            help="show progress during transfer",
            action="store_true",
        )
        parser.add_argument(
            "section", help="process only configuration sections specified", nargs="*"
        )

        parser.add_argument(
            "--drift",
            help="Compensate for the delta of time in seconds that passes between check and execution of rsync",
            type=int,
            default=1,
        )

        args = parser.parse_args()

        if args.stop_time:
            try:
                hour = int(args.stop_time.split(":")[0])
                minute = int(args.stop_time.split(":")[1])
                args.stop_time = datetime.now().replace(hour=hour, minute=minute)
            except Exception:
                print(
                    "ERROR: unable to parse supplied stop time value (example: HH:MM)"
                )
                exit(1)
        config = self.read_config(args.config_file)

        self.process_config(config, args)


def main():
    Application()


if __name__ == "__main__":
    main()
